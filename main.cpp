// Cleans up directories for media storage and removes irrelevant files.
// Copyright (C) 2018 Gavin Fuller
// This program is free software : you can redistribute it and / or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

//Special Thanks to:
//Kaldi, the Ethiopian goat herder, for finding coffee.
//Dennis Ritchie, for writing C. Everything after was a mistake.
//Linus Torvalds, for writing the Linux kernel. This is like 2 commands or one well written cron job in GNU/Linux.

//Headers
#include <stdlib.h> //For system commands
#include <iostream> //For cout output
#include <string> //For user input
#include <algorithm> //TODO: Remember what I used this header for
#include <windows.h> //For sleep function
#include <iomanip> //For config
#include <cstdlib> //For config
#include <fstream> //For config
#include <libconfig.h++> //For config
#include <vector>//For config

using namespace std;
using namespace libconfig; //For config

vector<string> filetypes;
vector<string> directoriess;

//DONE: Add ability to add and delete new file extensions
//DONE: Overhaul the file extension handling to make it easier to adjust file types on the fly
//DONE: Add configuration file so settings can be saved and adjusted
//DONE: Add ability to run multiple directories and add new ones
//CHANGE: Added /d to cd command to cut down on commands needed to complete task
//DONE: Add filter on config file to prevent inject attacks

char startcommand[50];
char finalcommand[50];
char directory[50];
string dletter;
bool setting(0);
int minmode;

bool mainmenu();

bool inputhandler() {
	string input;
	cin >> input;
	input.resize(1);
	transform(input.begin(), input.end(), input.begin(), ::tolower);
	cout << input << endl;
	if (input == "y") {
		return true;
	}
	else if (input == "n") {
		cout << "Goodbye!\n";
		exit(0);
	}
	else {
		cout << "Sorry, I did not understand that, please answer in a y/n format.\n";
		return false;
	}
}

void intro() {
	cout << "Directory Cleaner Alpha Build 4\nWritten by Gavin Fuller\nLicensed under (c) GNU GPL v3. See https://www.gnu.org/licenses for more information.\nNo warranties included, it WILL delete things you don't want it to if you do not triple check the configuration.\nThis is your last chance to exit, by proceeding you agree to accept the outcome of whatever happens.\n";
	cout << "Continue? (y/n): ";
	while (!inputhandler()); //Locks you into either responding yes or quitting
}

void extensioncompleter(int b);

void extensionhandler(int a, int z) {
	string ext = filetypes[a];
	cout << endl << "[" << a + 1 << "|" << z + 1 << "] Clearing ." << ext << " files.\n";
	ext.insert(0, "del /S *."); //Insert function is important here because I spent a good hour try to figure out how to do this with append, which is conveniently the wrong way to do it.
	strcpy_s(startcommand, ext.c_str());
	for (int b(0); (b < (int)directoriess.size()); b++) { //Figures out the number of times for the directory loop to execute, this means number of file types you want to use, minus one.
		extensioncompleter(b);
		cout << "\nConsole Window:\n" << finalcommand << endl; //Shows the console input so the user can transparently see processes running
		system(finalcommand);
	}
}

void run() {
	//config();
	const int z = (filetypes.size() - 1); //Figures out the number of times for the file extension loop to execute, this means number of file types you want to use, minus one.
	cout << "Searching for files now...\n";
	for (int a(0); a <= z; a++) { //a represents the number of file types to be cleared, currently set to run (z + 1) times
		extensionhandler(a, z);
	}
}

void dirset(int b) {
	string finaldir;
	dletter = "cd /d ";
	dletter.append(directoriess[b]);
	dletter.append(" && ");
	strcpy_s(directory, dletter.c_str());
}

void extensioncompleter(int b) {
	dirset(b);
	strcpy_s(directory, dletter.c_str());
	strcpy_s(finalcommand, directory); // Specify directory here, first is the drive letter, second is the folder
	strcat_s(finalcommand, startcommand); // append string two to the result
}

void intervalmenu() {
	system("cls");
	cout << "time Interval\n";
	cout << "Enter days to wait before running\t";
	int input;
	cin >> input;
	run();
	while (true) {
		Sleep(((1000 * 86400)*input)); //Math is for miliseconds to days
		run();
	}
}

void filehandler() {
	cout << "Would you like to add or remove a file type?\n0) Add\t1) Remove\t";
	bool input;
	cin >> input;
	if (!input) {
		cout << "What file type would you like to add?\n";
		string uinput;
		cin >> uinput;
		transform(uinput.begin(), uinput.end(), uinput.begin(), ::tolower);
	}
}

void menuinterp(int input) {
	switch (input) {
	case 1:
		run();
		system("pause");
		mainmenu();
		break;
	case 2:
		intervalmenu();
		break;
	case 3:
		exit(0);
		break;
	default:
		cout << "Input not understood.\n";
		break;
	}
}

bool mainmenu() {
	system("cls");
	cout << "Directory Cleaner Alpha Build 4\n1) Run\t\t2)Time Interval Run\t3) Exit\n";
	int input;
	cin >> input;
	if (input > 0 && input < 4) {
		menuinterp(input);
	}
	while (input <= 0 || input > 3) {
		cout << "What was that?\n";
		cin >> input;
	}
	return true;
}

void vectorout(vector<string> filetypes) {
	string abc;
	int arrsize((int)filetypes.size());
	for (int i = 0; i < (int)filetypes.size(); i++) {
		abc = filetypes.at(i);
		cout << abc << endl;
	}
}

bool isForbidden(char c)
{
	static string forbiddenChars("`~!@#$%^&*()-_=+{[]}':;'\"/<,.>?");
	return string::npos != forbiddenChars.find(c);
}

void config() {
	Config cfg;

	// Read the file. If there is an error, report it and exit.
	try {
		cfg.readFile("config.cfg");
	}

	catch (const FileIOException &fioex) {
		cerr << "I/O error while reading file." << endl;
	}

	catch (const ParseException &pex) {
		cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine()
			<< " - " << pex.getError() << endl;
	}

	catch (const SettingNotFoundException &nfex) {
		cerr << "No 'name' setting in configuration file." << endl;
	}

	try {
		minmode = cfg.lookup("minimalmode");
		if (minmode != 1)
			minmode = 0;
		cout << "Minimal Mode: " << minmode << endl;
	}
	catch (const SettingNotFoundException &nfex) {
		// Ignore.
	}

	// Output a list of all fts in the files root.
	try {
		for (int i = 0; i < cfg.getRoot()["files"].getLength(); ++i) {
			// Only output the record if all of the expected fields are present.
			string filetype;
			if (!(cfg.getRoot()["files"][i].lookupValue("filetype", filetype)))
				continue;
			replace_if(filetype.begin(), filetype.end(), isForbidden, ' ');
			filetypes.emplace_back(move(filetype));
		}
	}
	catch (const SettingNotFoundException &nfex) {
		// Ignore.
	}

	//Proof that all values have been properly stored.
	for (string const& filetype : filetypes) {
		//see if you can delete this now
	}

	// Output a list of all fts in the files.
	try {
		for (int i = 0; i < cfg.getRoot()["dirroot"].getLength(); ++i) {
			// Only output the record if all of the expected fields are present.
			string dir;
			if (!(cfg.getRoot()["dirroot"][i].lookupValue("dir", dir)))
				continue;
			replace_if(dir.begin(), dir.end(), isForbidden, ' ');
			directoriess.emplace_back(move(dir));
		}
	}
	catch (const SettingNotFoundException &nfex) {
		// Ignore.
	}
}



int main() {

	config();

	if (minmode == 1) {
		ShowWindow(GetConsoleWindow(), SW_HIDE); //SW_RESTORE to bring back
		run();
	}

	else {
		system("pause");
		intro();
		while (mainmenu());
		cout << "Task completed.\n"; //Doesn't check to see if anything went wrong
		system("pause");
	}

	if (minmode == 1)
		ShowWindow(GetConsoleWindow(), SW_RESTORE); //SW_RESTORE to bring back
	return 0; //Signal to the computer that the program has ended
}
